---
title: 🎆 Pull Requests
sidebar_label: 🎆 Pull Requests
description: This document contains information about 🎆 Pull Requests.
---



We welcome both pull requests and issues on GitHub. Bug fixes are encouraged.

Pull requests should be against the most current `master` branch.


---
title: 💭 Join our Discord
sidebar_label: 💭 Join our Discord
sidebar_position: 2
description: This document contains information about 💭 Join our Discord.
---



Join the official sunnypilot Discord server to stay up to date with all the latest features and be a part of shaping the future of sunnypilot!
* https://discord.sunnypilot.com

  ![](https://dcbadge.vercel.app/api/server/wRW3meAgtx?style=flat) ![Discord Shield](https://discordapp.com/api/guilds/880416502577266699/widget.png?style=shield)


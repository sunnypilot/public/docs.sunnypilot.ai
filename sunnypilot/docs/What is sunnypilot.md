---
title: 🌞 What is sunnypilot?
sidebar_label: 🌞 What is sunnypilot?
sidebar_position: 1
description: This document contains information about 🌞 What is sunnypilot?.
---



[sunnypilot](https://github.com/sunnyhaibin/sunnypilot) is a fork of comma.ai's openpilot, an open source driver assistance system. sunnypilot offers the user a unique driving experience for over 250+ supported car makes and models with modified behaviors of driving assist engagements. sunnypilot complies with comma.ai's safety rules as accurately as possible.


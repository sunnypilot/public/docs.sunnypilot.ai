---
title: 🛠️ Custom Stock
---

**Custom Stock** allows sunnypilot to manipulate and take over the set speed on the car's dashboard while using stock Adaptive/Smart Cruise Control.

:::tip Supported Cars
- Hyundai/Kia/Genesis
  - CAN platform
  - CAN-FD platform with `0x1CF` broadcasted in CAN traffic
- Honda Bosch
- Volkswagen MQB
  :::

:::info How to use Custom Longitudinal Control on sunnypilot
To use Custom Longitudinal Control on sunnypilot, follow these steps:

- **Setting the MAX ACC Speed:**
  - When using Speed Limit, Vision, or Map based Turn control, you will set the "MAX" ACC speed on the sunnypilot display, not on the dashboard.
  - The car will adjust the ACC setting in the dashboard to the targeted speed but won't exceed the max speed set on the sunnypilot display.

- **Adjusting Speed:**
  - A quick press of the RES+ or SET- buttons changes the speed by 5 MPH or KM/H on the sunnypilot display.
  - A long, deliberate press (about a 1/2 second) changes it by 1 MPH or KM/H.
:::note Note
Do not hold the RES+ or SET- buttons for longer than a second. Use either quick or long deliberate presses.
:::
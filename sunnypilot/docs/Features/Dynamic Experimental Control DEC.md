---
title: 🧪 Dynamic Experimental Control (DEC)
---

The **Dynamic Experimental Control (DEC)** feature enhances the driving experience by allowing supported cars to switch between Adaptive Cruise Control (ACC) and Blended mode dynamically. This is done based on the fetched map data and the confidence in the model. In other words, DEC intelligently chooses the best control method to ensure a smooth and safe ride.

- **ACC Mode**: In this mode, the car uses traditional Adaptive Cruise Control for longitudinal planning.
- **Blended Mode**: This experimental longitudinal control method provides a more customized and dynamic response based on real-time conditions.

:::info Supported Cars
- sunnypilot Longitudinal Control capable
- Stock Longitudinal Control
  * Hyundai/Kia/Genesis (non CAN-FD)
  * Honda Bosch
  * Volkswagen MQB
:::

:::tip Acknowledgement
Big thanks to the Move Fast team for the amazing implementation of the DEC feature!
:::

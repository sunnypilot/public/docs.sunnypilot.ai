---
title: 🛣️ Dynamic Lane Profile (DLP)
---

**Dynamic Lane Profile (DLP)** aims to provide the best driving experience by confidently keeping the vehicle within a lane. It allows sunnypilot to dynamically switch between lane profiles based on the lane recognition confidence level on the road.


<details>
  <summary>**Available Modes**</summary>

- **Auto Lane**: sunnypilot dynamically chooses between `Laneline` or `Laneless` model.
- **Laneline**: sunnypilot uses the Laneline model only.
- **Laneless**: sunnypilot uses the Laneless model only.
</details>

:::info Steps to use Dynamic Lane Profile
1. Navigate to sunnypilot Settings -> `SP - Controls`.
2. Enable Dynamic Lane Profile by toggling to ON.
3. Reboot the system.
4. Before driving, toggle between the 3 modes on the on-road camera screen by pressing the button.
5. Start driving.
   :::

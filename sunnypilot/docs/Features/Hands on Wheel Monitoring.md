---
title: 👐 Hands on Wheel Monitoring
---

:::info
**Toggle Hands on Wheel Monitoring** monitors and alerts the driver when their hands have not been on the steering wheel for an extended time.
:::

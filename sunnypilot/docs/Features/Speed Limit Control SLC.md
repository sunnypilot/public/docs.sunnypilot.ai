---
title: 🚦 Speed Limit Control (SLC)
---

**Speed Limit Control (SLC)** automatically adapts the cruise speed to road limits by using speed limit signs information from map data and the car interface.

:::note
**HKG only**: Highway Driving Assist (HDA) status integration is available on applicable HKG cars.
:::

**SLC Supports Selecting Different Policies:**  
SLC allows users to choose the source of speed limit data that will be used to adjust the cruise speed. The options are:

- **Nav Only**: Uses navigation data from a loaded Mapbox route to determine speed limits.
- **Map Only**: Relies solely on OpenStreetMap (OSM) data for speed limit information.
- **Car Only**: Utilizes the car's built-in data, such as camera or navigation system, to determine speed limits.
- **Nav First**: Gives priority to navigation data, followed by map data, and finally car data.
- **Map First**: Prioritizes map data, followed by navigation data, and then car data.
- **Car First**: Prioritizes car data, followed by navigation data, and then map data.

<details>
  <summary>OpenStreetMap (OSM) Speed Limit Prediction</summary>

Any option that uses OSM data is capable of predicting the next speed limit for a more natural drive, dependent on having offline data or onboard connectivity via SIM or hotspot. **However, when OSM is not the priority source, the prediction will only kick in if the priority source returns no information regarding the speed limit.**
</details>

:::info Speed Limit Offset
When Speed Limit Control is enabled, you can set the speed limit offset to be either a percentage (%) or a static value:

- **Percentage Offset**: Adjusts the speed limit by a certain percentage above the actual speed limit.
- **Static Offset**: Adds a fixed value to the actual speed limit.

This allows for a more natural drive while still adhering closely to road regulations.
:::

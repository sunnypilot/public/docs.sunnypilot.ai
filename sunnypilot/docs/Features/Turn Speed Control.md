---
title: 🔄 Turn Speed Control
---

**Turn Speed Control (TSC)** ensures safe and comfortable driving through turns by intelligently adjusting the vehicle's speed. TSC provides two distinct options: **Vision-based Turn Speed Control (V-TSC)** and **Map-Data-based Turn Speed Control (M-TSC)**.

### V-TSC
:::tip
_**V**ision-based **T**urn **S**peed **C**ontrol_ <br/>Uses vision path predictions to estimate the appropriate speed for upcoming turns, ensuring a smoother driving experience.
:::

### M-TSC
:::tip
_**M**ap-based **T**urn **S**peed **C**ontrol_ <br/>Leverages map data to define speed limits for turns, taking into account the curvature of the road.
:::

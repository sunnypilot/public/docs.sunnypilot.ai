---
title: ✨ Features
---

Dive into the world of sunnypilot 🚗, where we bring you a suite of features designed to enhance your driving experience. From customizing cruise control to intelligent speed adjustments, each feature is crafted with precision and care 🛠️. We hope you enjoy these enhancements as much as we enjoyed creating them 💙.

## **🌟 Feature Highlights:**
- [**🛠️ Custom Stock**](./Custom%20Stock): Tailor your cruise control settings to your preferences.
- [**🧪 Dynamic Experimental Control (DEC)**](./Dynamic%20Experimental%20Control%20DEC): Switch seamlessly between ACC and Blended mod.
- [**🛣 Dynamic Lane Profile (DLP)**](./Dynamic%20Lane%20Profile%20DLP): Adjust the lane-keeping profile dynamically.
- [**➡️🚗️ Gap Adjust Cruise (GAC)**](./Gap%20Adjust%20Cruise%20GAC): Adjust the cruise gap distance dynamically.
- [**👐 Hands on Wheel Monitoring**](./Hands%20on%20Wheel%20Monitoring): Monitor hands-on-wheel engagemen.
- [**🛡️ Modified Assistive Driving Safety (MADS)**](./Modified%20Assistive%20Driving%20Safety%20MADS): Modify driving assist engagement behavior.
- [**🚦️ Speed Limit Control (SLC)**](./Speed%20Limit%20Control%20SLC): Adapt cruise speed based on road limits.
- [**🔄 Turn Speed Control**](./Turn%20Speed%20Control): Adjust your vehicle's speed intelligently through turn.

Explore each section to learn more about these features and how to use them effectively 🧐.

:::info
**📡 Data Connection Requirements:**  
Certain features, such as [**Turn Speed Control**](./Turn Speed Control) and _some policies_ of [**Speed Limit Control%20(SLC)**](./Speed%20Limit%20Control%20SLC), are only available with an active data connection. This can be established via:
- [**Comma Prime**](https://comma.ai/prime): An intuitive service provided directly by Comma, or
- **Personal Hotspot**: From your mobile device or a dedicated hotspot from a cellular carrier 📱.
  :::

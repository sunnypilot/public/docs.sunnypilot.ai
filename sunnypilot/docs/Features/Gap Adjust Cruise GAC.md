---
title: ➡️🚗 Gap Adjust Cruise (GAC)
---

**Gap Adjust Cruise (GAC)** allows users to adjust the cruise gap distance using the cruise gap buttons in the car for supported openpilot longitudinal cars.

:::info
Please proceed with caution and understand the limitations of the system.
:::

## Modes

There are 4 modes available, but only 3 can be toggled via the steering wheel:

[//]: # (- **Relaxed Gap**: This mode can only be selected via the on-road camera screen and is not available for toggling via the steering wheel.)
- **Stock Gap**: Stock sunnypilot distance with a 1.45-second profile.
- **Mild Gap**: Semi-aggressive distance with a 1.25-second profile.
- 🚨**Aggro Gap**🚨: Aggressive distance with a 1.0-second profile.

<details>
  <summary>Availability</summary>

| Car Make            | Stock Gap | Mild Gap | Aggro Gap | Relaxed Gap |
  |---------------------|-----------|----------|-----------|-------------|
| Honda/Acura         | ✅         | ✅       | ✅        | ✅           |
| Hyundai/Kia/Genesis | ✅         | ✅       | ✅        | ✅           |
| Toyota/Lexus        | ✅         | ✅       | ✅        | ✅           |
| Volkswagen MQB/PQ   | ✅         | ✅       | ✅        | ✅           |
</details>
